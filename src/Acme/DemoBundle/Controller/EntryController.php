<?php

namespace Acme\DemoBundle\Controller;
use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\BlogEntry;
use Symfony\Component\HttpFoundation\Request;

class EntryController extends Controller {
    
    public function indexAction($category = null) {   
        
        $categories = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Category')
                ->findAll();
        
        if($category != null) { 
            $repository = $this->getDoctrine()->getRepository('AcmeDemoBundle:BlogEntry')
                    ->findBy(array(
                        'category' => $category
                    ));
        }
        else { 
            $repository = $this->getDoctrine()->getRepository('AcmeDemoBundle:BlogEntry')
                    ->findAll();                 
        }
        
        return $this->render('AcmeDemoBundle:Entry:index.html.twig',array(
            'BlogEntries' => $repository,
            'Categories'  => $categories,
            'title'       => 'Lista wpisów bloga'
        ));
    }
    
    public function createAction($entryId = null,Request $request) { 
        $authorsData = array();
        $categoryData= array();
        
        if($entryId) {
            $entry = $this->getDoctrine()->getRepository('AcmeDemoBundle:BlogEntry')
                    ->find($entryId);
            
            if($entryId == null) { 
                throw $this->createNotFoundException();
            }
        }
        else { 
            $entry = new BlogEntry;
        }
        $authors = $this->getDoctrine()->getRepository('AcmeDemoBundle:Author')
                ->findAll();
        $categories = $this->getDoctrine()->getRepository('AcmeDemoBundle:Category')
                ->findAll();
        
        foreach($authors as $author) { 
            $authorsData[$author->getId()] = $author->hello();
        }
        foreach($categories as $cateogry) { 
            $categoryData[$cateogry->getId()] = $cateogry->getName();
        }
        // Formularz
        
        $form = $this->createFormBuilder(new BlogEntry())
                ->add('title','text',array('label' => 'Tytuł'))
                ->add('author','choice',array(
                    'choices' => $authorsData
                ))
                ->add('category','choice',array(
                    'choices' => $categoryData
                ))
                ->add('content','textarea',array('label' => 'Zawartość'))
                ->add('save','submit',array('label' => 'Dodaj wpis'))
                ->getForm();
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $entry = $form->getData();
            $author = $this->getDoctrine()->getRepository('AcmeDemoBundle:Author')
                            ->find($form->get('author')->getData());
            $category = $this->getDoctrine()->getRepository('AcmeDemoBundle:Category')
                    ->find($form->get('category')->getData());

            //Dodawanie obiektu autora   
            $entry->setAuthor($author);
            // Dodawanie obiektu kategorii 
            $entry->setCategory($category);
            $entry->setCreatedDate(new \DateTime);
            $entry->setLastEditDate(new \DateTime);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($entry);
            $em->flush();
            
            if($entry->getId()) { 
                $this->addFlash('notice','Wpis został dodany do bazy danych');
                return $this->redirectToRoute('blog_entries_show');
            }
        }

        return $this->render('AcmeDemoBundle:Entry:create.html.twig',array(
            'form' => $form->createView(),
            'title'=> 'Dodaj nowy wpis'
        ));
        
    }
    

}