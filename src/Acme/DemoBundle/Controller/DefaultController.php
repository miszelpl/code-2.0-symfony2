<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name = null)
    {
//        try { 
            $categories = array(
                '1' => 'Pieski',
                '2' => 'Kotki',
                '3' => 'Programowanie'
            );

            if($name == 'Pieski') { 
                $subCategories = array(
                    'yorki','chow chow','dobrmany','jamniki'
                );
            }

            $image = '';
            if($name == 'a') {
                $image = 'http://cache.reelz.com/assets/content/blogimages/chucknorris-MIA2.jpg';
            }
            elseif($name == 'b') { 
                $image = 'http://www.youngcons.com/wp-content/uploads/2015/04/Chuck-Norris.jpg';
            }
            
            return $this->render('AcmeDemoBundle:Default:index.html.twig',
                    array('title'       => $name,
                        'categories'    => $categories,
                        'subCategories' => isset($subCategories) ? $subCategories : null,
                        'image'         => $image
                    )
            );
    }
    
    public function showAction() { 
        
    }
    
    public function helloAction($name) {
        
        return new Response('<p>Eloszka '.$name.'</p>');
    }
    
    public function surnameAction($name =null,$surname = null,Request $request) {
        
        echo $_GET['id'].'<br>';
        
//        echo $request->query->.'<br>';
        echo $request->get('id');
        
        return $this->redirectToRoute('acme_demo_homepage');
    } 
    
    
}
