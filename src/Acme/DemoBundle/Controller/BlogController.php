<?php
namespace Acme\DemoBundle\Controller;
use \Symfony\Bundle\FrameworkBundle\Controller\Controller;


class BlogController extends Controller
{
    /**
     * 
     * @param String $name Nazwa kategorii. Nieznaczące
     * @param Integer $id Id kategorii bloga
     * @return type
     */
    public function indexAction($name =null ,$id = null) { 
        
        $entryRepository = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:BlogEntry');
        $categoryRepository = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Category');
        
        if($id) { 
            $entries = $entryRepository->findBy(array(
                'category' => $id
            ), array('id' => 'desc'));
        }
        else { 
            $entries = $entryRepository
                    ->findBy(array(), array('id' => 'desc'));
        }
        
        return $this->render('AcmeDemoBundle:Blog:index.html.twig',array(
            'Entries'   => $entries,
            'Categories'=> $categoryRepository->findAll(),
            'title'     => 'Strona główna'
        ));
        
    }
    
    public function readAction($id = null) { 
        
        $categoryRepository = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Category');
        
        $entry = $this->getDoctrine()->getRepository('AcmeDemoBundle:BlogEntry')
                ->find($id);
        
        return $this->render('AcmeDemoBundle:Blog:read.html.twig',array(
            'Entry'         => $entry,
            'Categories'    => $categoryRepository->findAll(),
            'title'         => $entry->getTitle()
        ));
        
    }
    
}
