<?php

namespace Acme\DemoBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\DemoBundle\Entity\Category;
use Acme\DemoBundle\Factory\CategoryFactory;

use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller {
    
    public function indexAction(Request $request) {
        
        $doctrine = $this->getDoctrine()->getRepository('AcmeDemoBundle:Category');
        
        $searchForm =  $this->createForm('form')
                        ->add('searchString','text',array('label' => 'Szukaj'))
                        ->add('search','submit',array('label' => 'Szukaj'));  
        
        $searchForm->handleRequest($request);
        if($searchForm->isSubmitted()) { 
            $searchData = $searchForm->getData();
            $data = $doctrine->findBy(array(
                'name' => $searchData['searchString']
            ));
        }
        else { 
            $data = $doctrine->findAll();
        }
        
        
        return $this->render('AcmeDemoBundle:Category:index.html.twig',array(
            'categories'    => $data,
            'searchForm'    => $searchForm->createView(),
            'title'         => 'Lista kategorii Bloga'
        ));
        
    }
    
    public function createAction($id,\Symfony\Component\HttpFoundation\Request $request) { 
        $status = null;
        $manager = $this->getDoctrine()->getManager();
        
        $repository = $this->getDoctrine()->getRepository('AcmeDemoBundle:Category');
        $factory = new CategoryFactory($repository,$id);
        $category = $factory->create();
        
        // To jest potencjalna klasa CategoryForm
       
        $form = $this->createFormBuilder($category)
                ->add('name','text',array('label' => 'Nazwa kategorii'))
                ->add('Dodaj kategorię','submit',array('label' => 'Dodaj kategorię'))
                ->getForm();
        
        $form->handleRequest($request);
        if($form->isSubmitted()) { 
            if($form->isValid()) {  
                
                $category = $form->getData();
                $manager->persist($category);
                $manager->flush();
                
                $this->get('session')->getFlashBag()->add('notice','wszystko ok');
                return $this->redirectToRoute('blog_categories_show');
            }
        }
        
        return $this->render('AcmeDemoBundle:Category:create.html.twig',array(
            'form'      => $form->createView(),
            'status'    => $status
        ));   
    }
    
    public function removeAction($id) { 
        $category = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Category')->find($id);
        try {
            
            if(!$category) { 
                throw $this->createNotFoundException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            
            $this->addFlash('success','Usunąłem kategorię z bazy danych');
        }
        catch(\Exception $e) {             
            $this->addFlash('notice','Nie udało się usunać kategorrii. Błąd '.$e->getMessage());
        }
        
        
        return $this->redirectToRoute('blog_categories_show');
    }
    
}
