<?php
namespace Acme\DemoBundle\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use \Acme\DemoBundle\Entity\Author;

use Symfony\Component\HttpFoundation\Response;
use \Symfony\Component\HttpFoundation\Request;
class AuthorController extends Controller {
    
    // Tu można by to wykorzystać ? 
    protected $repositoryName = null;
    

    protected $repository = null;

    public function indexAction(Request $request) {

        $form = $this->createFormBuilder(new Author())
                        ->add('firstName','text')
                        ->add('lastName','text')
                        ->add('email','text')
                        ->add('save','submit',array('label' => 'Dodaj nowego autora'))
                        ->getForm(); // !!! Ważne -> stwórz fortmularz
        
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $author = $form->getData();

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($author);
            $manager->flush();
            
            if($author->getId()) { 
                $this->addFlash('success','Autor został dodany');
            }
            else { 
                $this->addFlash('error','Nie można dodać autora do bazy');
            }
        }
        
        $searchForm =  $this->createForm('form')
                        ->add('searchString','text',array('label' => 'Szukaj'))
                        ->add('search','submit',array('label' => 'Szukaj'));  
        
        $searchForm->handleRequest($request);
        
        $repo = $this->getDoctrine()->getRepository('AcmeDemoBundle:Author');
        
        if($searchForm->isSubmitted() && $searchForm->isValid()) { 
            
            $formData = $searchForm->getData();

            $data = $repo->findBy(array(
                'firstName' => $formData['searchString']
            ), array(
                'id' => 'DESC'
            ));
        }
        else { 
            $data = $repo->findAll();                
        }
        
        return $this->render('AcmeDemoBundle:Author:index.html.twig',array(
            'Authors'   => $data,
            'title'     => 'Lista autorów bloga',
            'form'      => $form->createView(),
            'searchForm'=> $searchForm->createView()
        ));
    }
    
    public function addAction() { 
        
        $manager = $this->getDoctrine()->getManager();
        
        $author = new Author();
        $author->setEmail('ee@sdsds.pl')
                ->setFirstName('Marek')
                ->setLastName('Joł');   
        
        $manager->persist($author);
        $manager->flush();
        
        return new Response($author->getId());
    }
    
    public function updateAction($id) {
        
        try { 
//            $author = $this->getDoctrine()
//                    ->getRepository('AcmeDemoBundle:Author')
//                    ->find($id);

            $author = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Author')->find($id);            
            
            if(!$author) {
                throw $this->createNotFoundException();
            }
            
            $manager = $this->getDoctrine()
                    ->getManager();
            
            $author->setEmail('michal@buziaczek.pl');
            
            $manager->persist($author);
            $manager->flush();
            
            
            return new Response(print_r($author,1));            
        }
        catch (\Exception $e) { 
            return new Response('Lipa');
        }
        
    }
    
    public function removeAction($id) { 
        $author = $this->getDoctrine()
                ->getRepository('AcmeDemoBundle:Author')->find($id);
        try {
            
            if(!$author) { 
                throw $this->createNotFoundException();
            }
            $em = $this->getDoctrine()->getManager();
            $em->remove($author);
            $em->flush();
            
            $this->addFlash('success','Usunąłem autora z bazy danych');
        }
        catch(\Exception $e) {             
            $this->addFlash('notice','Nie udało się usunąć wpisu. Błąd '.$e->getMessage());
        }
        
        
        return $this->redirectToRoute('blog_author_show');
    }
    
}