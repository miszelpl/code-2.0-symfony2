<?php

namespace Acme\DemoBundle\Factory;
use Acme\DemoBundle\Entity\Category;

class CategoryFactory {
    
    /**
     *
     * @var Doctrine\ORM\EntityRepository
     */
    protected $repository = null;
    /**
     *
     * @var Integer 
     */
    protected $id = null;
    
    public function __construct(\Doctrine\ORM\EntityRepository $repository,$id = null) {
        $this->repository = $repository;
        $this->id = $id;
    }
    /**
     * 
     * @return \Category
     */
    public function create() { 
        if($this->id != null) { 
//            $em = $this->getDoctrine()->getRepository('AcmeDemoBundle:Category');
            
            $category = $this->repository->find($this->id);

            if($category == null) { 
                $category = new Category;
            }
        }
        else { 
            $category = new Category;
        }        
        
        return $category;
    }
    
}