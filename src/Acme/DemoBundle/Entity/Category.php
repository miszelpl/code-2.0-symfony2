<?php

namespace Acme\DemoBundle\Entity;
use \Symfony\Component\Validator\Exception\InvalidArgumentException;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity(fields="name", message="category.name.unique")
 */
class Category {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */    
    protected $id;
    
    /**
     * @ORM\Column(type="string", unique=true)
     */
    protected $name;
    
    /**
     * @ORM\OneToMany(targetEntity="\Acme\DemoBundle\Entity\BlogEntry", mappedBy="BlogEntry")
     */    
    protected $entries = array();

    public function setName($name) { 
        $this->name = $name;
        
        return $this;
    }
    
    public function setEntry(BlogEntry $entry) { 
        $this->entries[] = $entry;
        
        return $this;
    }
    
    public function setEntries() { 
        $entries = func_get_args();
        /**
         * @param BlogEntry $entry 
         */
        foreach($entries as $entry) {
            if($entry instanceof BlogEntry) { 
                $this->entries[] = $entry;
            }
            else { 
                throw InvalidArgumentException('Argument musi być instancją BlogEntry');
            }
        }
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function getId() { 
        return $this->id;
    }
    /**
     * Trzeba by jeszcze polskie znaki usunąć. Mryg
     * @return type
     */
    public function removeInvalidChars() { 
        return str_replace(array(' ','ą'),array('_','a'),$this->name);
    }
}