<?php

namespace Acme\DemoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Author {
    /**
     * 
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
    protected $firstName;
    /**
     * @ORM\Column(type="string")
     */    
    protected $lastName;
    /**
     * @ORM\Column(type="string")
     */
    protected $email;
   
    /**
     * @ORM\OneToMany(targetEntity="\Acme\DemoBundle\Entity\BlogEntry", mappedBy="BlogEntry")
     */
    protected $entries = array();
    
    /**
     * 
     * @param String $name
     * @return \Author
     */
    public function setFirstName($name){
        $this->firstName =$name;
        return $this;
    }
    
    /**
     * 
     * @param String $lastName
     * @return \Author
     */
    public function setLastName($lastName){
        $this->lastName=$lastName;
        return $this;
    }
    /**
     * 
     * @param type $email
     * @return \Author
     */
    public function setEmail($email) { 
        $this->email = $email;
        
        return $this;
    }
    
    /**
     * 
     * @return String
     */
    public function getEmail() { 
        return $this->email;
    }
    
    public function getFirstName() { 
        return $this->firstName;
    }
    
    public function getLastName() { 
        return $this->lastName;
    }
    
    public function setId($id) { 
        $this->id = $id;
    }
    
    public function getId() { 
        return $this->id;
    }
    
    public function hello() { 
        return $this->firstName.' '.$this->lastName;
    }
}
