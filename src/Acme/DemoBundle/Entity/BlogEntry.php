<?php
namespace Acme\DemoBundle\Entity;

use \Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class BlogEntry {
    
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    protected $title;
    /**
     *
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdDate;
    /**
     *
     * @ORM\Column(type="datetime", name="last_edited_at")
     */    
    protected $lastEditDate;
    /**
     * @ORM\ManyToOne(targetEntity="Author")
     * @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     */
    protected $author;
    /**
     * @ORM\Column(type="text")
     */
    protected $content;
    
    /**
     * @ORM\ManyToOne(targetEntity="Category")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    protected $category; 
    
    public function setTitle($title) {
        
        if($title == null) { 
            throw new InvalidArgumentException('Tytuł nie może być pusty');
        }
       
        $this->title = $title;
        return $this;
    }
    
    public function setCreatedDate(\DateTime $date) { 
        $this->createdDate = $date;
        
        return $this;
    }
 

    
    public function getId() { 
        return $this->id;
    }
    
    public function setId($id) { 
        $this->id = $id;
        
        return $this;
    }    

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set lastEditDate
     *
     * @param \DateTime $lastEditDate
     * @return BlogEntry
     */
    public function setLastEditDate(\DateTime $lastEditDate)
    {
        $this->lastEditDate = $lastEditDate;

        return $this;
    }

    /**
     * Get lastEditDate
     *
     * @return \DateTime 
     */
    public function getLastEditDate()
    {
        return $this->lastEditDate;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return BlogEntry
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get author
     *
     * @return \Acme\DemoBundle\Entity\Author 
     */
    public function getAuthor()
    {
        return $this->author;
    }
   
    public function setAuthor($author) { 
        $this->author = $author;
        
        return $this;
    }    
    
    public function setCategory($category) { 
        $this->category = $category;
        return $this;
    }
    
    public function getCategory() { 
        return $this->category;
    }
    
    public function trim($length = 255) { 
        return substr($this->content,0,$length);
    }
    
}
